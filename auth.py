from flask_login import current_user, login_user, logout_user
from .models import User
from . import app

from flask import render_template, flash, redirect, request, url_for

from .forms import LoginForm


def field_validator(name):
    if name == "" or name == None:
        return False;
    if type(name) == str:
        name = name.strip()
    if len(name) < 2 or len(name) > 20:
        return False
    return True

@app.route('/login', methods=['GET', 'POST'])
def login():
    
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    
    # print (form.username.data);
        
    if request.method == 'POST' and field_validator(form.username.data) and field_validator(form.password.data):
        user = User.query.filter_by(username=form.username.data).first()

        if user is None:
            flash('نام کاربری یا رمز عبور اشتباه است')
            return redirect(url_for('login'))
        if user.password == form.password.data:
            login_user(user)
            return redirect(url_for('main.index'))

    if request.method == 'POST' and ( field_validator(form.username.data) == False or  field_validator(form.password.data) == False ):
        flash('نام کاربری یا رمز عبور را درست وارد کنید.')
        return redirect(url_for('login'))


    # elif request.method == 'POST':
        # flash('Invalid username or password')
        
    # flash("Hello")
    return render_template('login.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))