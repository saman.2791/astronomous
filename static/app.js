            $('#reset_form').click(function(){
              $('input').val('');
            });
$('#autocomplete').autocomplete({
    serviceUrl: '/locations',
    onSelect: function (suggestion) {
            $("#autocomplete_hidden").val(suggestion.value);
            var coord = suggestion.value.substring(
                suggestion.value.lastIndexOf("(") + 1, 
                suggestion.value.lastIndexOf(")")
            );
          
            lonlat = coord.split(",");
            lon = lonlat[0];
            lat = lonlat[1].trim();
            $("#d_lon").val(lon);
            $("#d_lat").val(lat);
        }
});


$("#manual_location").click(function(){

  $('#manualLocationLon').val('')
  $('#manualLocationLon').toggle();
  $('#manualLocationLat').val('')
  $('#manualLocationLat').toggle();
  $('#autocomplete').val('')
  $('#autocomplete').toggle();
});

            $('#get_chart').click(function(){

                
                if($("#fname").val() == ""){
                  $("#fname").removeClass('is-valid');
                  $("#fname").addClass('is-invalid');
                  return false;                  
                }
                if($("#fname").val() != ""){
                  $("#fname").removeClass('is-invalid');
                  $("#fname").addClass('is-valid');
                }
                                if($("#lname").val() == ""){
                  $("#lname").removeClass('is-valid');
                  $("#lname").addClass('is-invalid');
                  return false;                  
                }
                if($("#lname").val() != ""){
                  $("#lname").removeClass('is-invalid');
                  $("#lname").addClass('is-valid'); 
                }
                //if($.isNumeric($("#tz").val()) == false){
                //  $("#tz").removeClass('is-valid');
                //  $("#tz").addClass('is-invalid');
                //} 
                //if($.isNumeric($("#tz").val()) == true){
                //  $("#tz").removeClass('is-invalid');
                //  $("#tz").addClass('is-valid');
                //}
                
                if($.isNumeric($("#hour").val()) == false){
                  $("#hour").removeClass('is-valid');
                  $("#hour").addClass('is-invalid');
                  return false;                  
                } 
                if($.isNumeric($("#hour").val()) == true){
                  $("#hour").removeClass('is-invalid');
                  $("#hour").addClass('is-valid');
                }
                
                if($.isNumeric($("#min").val()) == false){
                  $("#min").removeClass('is-valid');
                  $("#min").addClass('is-invalid');
                  return false;
                } 
                if($.isNumeric($("#min").val()) == true){
                  $("#min").removeClass('is-invalid');
                  $("#min").addClass('is-valid');
                }
                
                if($.isNumeric($("#j_day").val()) == true){
                  $("#j_day").removeClass('is-invalid');
                  $("#j_day").addClass('is-valid');
                }                
                if($.isNumeric($("#j_day").val()) == false){
                  $("#j_day").removeClass('is-valid');
                  $("#j_day").addClass('is-invalid');
                  return false;                  
                }
                
                if($.isNumeric($("#j_month").val()) == true){
                  $("#j_month").removeClass('is-invalid');
                  $("#j_month").addClass('is-valid');
                }                
                if($.isNumeric($("#j_month").val()) == false){
                  $("#j_month").removeClass('is-valid');
                  $("#j_month").addClass('is-invalid');
                  return false;                  
                }
                
                if($.isNumeric($("#j_year").val()) == true){
                  $("#j_year").removeClass('is-invalid');
                  $("#j_year").addClass('is-valid');
                }                
                if($.isNumeric($("#j_year").val()) == false){
                  $("#j_year").removeClass('is-valid');
                  $("#j_year").addClass('is-invalid');
                  return false;                  
                }                

                if($.isNumeric($("#j_year").val()) == false){
                  $("#j_year").removeClass('is-valid');
                  $("#j_year").addClass('is-invalid');
                  return false;                  
                }
                
                if (($('#manualLocationLat')).val() != "" &&
                    ($('#manualLocationLat')).val().toUpperCase().includes("S") == false ||
                    ($('#manualLocationLat')).val().toUpperCase().includes("N") == false
                    ) {
                      $('#manualLocationLat').removeClass("is-valid");
                      $('#manualLocationLat').addClass("is-invalid");
                }

                if (($('#manualLocationLat')).val() != "" &&
                    ($('#manualLocationLat')).val().toUpperCase().includes("S") == true ||
                    ($('#manualLocationLat')).val().toUpperCase().includes("N") == true
                    ) {
                      $('#manualLocationLat').removeClass("is-invalid");
                      $('#manualLocationLat').addClass("is-valid");
                }
                
//

                if (($('#manualLocationLon')).val() != "" &&
                    ($('#manualLocationLon')).val().toUpperCase().includes("E") == false ||
                    ($('#manualLocationLon')).val().toUpperCase().includes("W") == false
                    ) {
                      $('#manualLocationLon').removeClass("is-valid");
                      $('#manualLocationLon').addClass("is-invalid");
                }
                if (($('#manualLocationLon')).val() != "" &&
                    ($('#manualLocationLon')).val().toUpperCase().includes("E") == true ||
                    ($('#manualLocationLon')).val().toUpperCase().includes("W") == true
                    ) {
                      $('#manualLocationLon').removeClass("is-invalid");
                      $('#manualLocationLon').addClass("is-valid");
                }
                
                else {
                 $('#collapseTwo').collapse('show');
                 //$('#collapseOne').collapse('close');
               }
            });


$("#save_chart").click(function(){
    var chart_data = {
      'fname': $("#fname").val(),
      'lname': $("#lname").val(),
      'fname': $("#fname").val(),
    };
    $.ajax({
      type: 'POST',
      url: '/chart_new',
      data: JSON.stringify (chart_data),
      success: function(data) { alert('data: ' + data); },
      contentType: "application/json",
      dataType: 'json'
  });
  
});