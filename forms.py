from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, validators, SelectField, HiddenField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):

    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField("Login")


class UserForm(FlaskForm):
    fname = StringField('First Name')
    lname = StringField('Last Name')
    username = StringField('Username')
    password = PasswordField('Password')
    term = SelectField('Term', choices =
                       [('1', 'ترم یک'),
                        ('2', 'ترم دوم'),
                        ('3', 'ترم سه'),
                        ('4', 'ترم چهار')])
    id = HiddenField()
    submit = SubmitField("Login")
