from flask_login import UserMixin
from . import db, login
from collections import OrderedDict
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True)
    fname = db.Column(db.String(120), index=True)
    lname = db.Column(db.String(120), index=True)
    password = db.Column(db.String(128))
    term = db.Column(db.Integer)

    charts = db.relationship('Chart', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)
    
@login.user_loader
def load_user(id):
    return User.query.get(int(id))    
    
class Chart(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    
    fname = db.Column(db.String(140))
    lname = db.Column(db.String(140))

    date_d = db.Column(db.String(140))
    date_m = db.Column(db.String(140))
    date_y = db.Column(db.String(140))

    date_gd = db.Column(db.String(140))
    date_gm = db.Column(db.String(140))
    date_gy = db.Column(db.String(140))

    date_type = db.Column(db.Integer)

    city = db.Column(db.String(140))
    lon = db.Column(db.String(140))
    lat = db.Column(db.String(140))

    birth_time_h = db.Column(db.String(140))
    birth_time_m = db.Column(db.String(140))

    tz = db.Column(db.String(140))

    lagna = db.Column(db.Integer)
    
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Chart {}>'.format(self.fname)
    
    
    
class Location(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    country = db.Column(db.String(140))
    city = db.Column(db.String(140))
    lon = db.Column(db.String(140))
    lat = db.Column(db.String(140))
    alt = db.Column(db.String(140))
    
    def to_dict(self):

        result = OrderedDict()
        for key in self.__mapper__.c.keys():
            if getattr(self, key) is not None:
                result[key] = str(getattr(self, key))
            else:
                result[key] = getattr(self, key)
        return result

    def __repr__(self):
        return '<Location {}>'.format(self.city)
    
    
class Timezone(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    
    abbr = db.Column(db.String(140))
    name = db.Column(db.String(140))
    utc_offset = db.Column(db.String(140))
    offset = db.Column(db.String(140))
    offset_second = db.Column(db.String(140))
    
    def __repr__(self):
        return '<Timezone {}>'.format(self.name)