# -*- coding: utf-8 -*-

# main.py
from flask import Blueprint, render_template, request, flash, redirect, url_for, request, Response, jsonify
from . import db
from .models import User, Location, Chart, Timezone
from flask_login import login_required, current_user
import json
from .forms import UserForm

main = Blueprint('main', __name__)

TERMS = ['یک'
         'دو',
         'سه',
         'چهار']



@main.route('/')
@login_required
def index():
    title = "خانه"
    return render_template('index.html', title=title)

@main.route('/chart-calculator')
@login_required
def chart_calculator():
    title = "محاسبه نمودار تولد"
    tzs = Timezone.query.all()
    return render_template('chart_calculator.html', title=title, tzs=tzs)


@main.route('/chart-new', methods=['GET', 'POST'])
@login_required
def chart_new():
    content = request.json
    return jsonify(content)

@main.route('/chart-list')
@login_required
def chart_list():
    title = "لیست نمودارهای تولد"
    print (vars(current_user));
    charts = Chart.query.filter_by(user_id=current_user.id).all()
    return render_template('list_chart.html', title=title, charts=charts)

@main.route('/locations', methods=["GET"])
@login_required
def locations():
    q = str(request.args.get('query')).rstrip().lower()
    print ("query")
    print (q)
    look_for = '%{0}%'.format(q)
    # locations = Location.query.all()
    locations = Location.query.filter(Location.city.ilike(look_for)).all()
    # print (locations);
    result = []
    for loc in locations:
         _json = {}
         _json['data'] = str(loc.id)
         _json['value'] = loc.country + " - " + loc.city + " (" + loc.lon + ", " + loc.lat +")"
         result.append(_json)
    _json= {"query": "Unit", "suggestions" : result}
    return jsonify(_json)
        
        
    

@main.route('/users',  methods=["GET", "POST"])
@login_required
def users():
    title = "لیست کاربران"
    users = User.query.all()
    user_form = UserForm()
    print(dir(user_form.term))
    if request.method == "POST" and user_form.submit():
        new_user= User(fname=user_form.fname.data,
                       lname=user_form.lname.data,
                       password=user_form.password.data,
                       username=user_form.username.data,
                       term=user_form.term.data)

        db.session.add(new_user)
        db.session.commit()
        flash("عضو جدید اضافه شد")
        return redirect(url_for('main.users'))

    return render_template('list_users.html', form=user_form, title=title, users=users)



#WIP: complete edit
@main.route('/users/edit',  methods=["GET", "POST"])
@login_required
def edit_user():
    title = "ویرایش کاربر"
    user_form = UserForm()
    print (user_form.id.data)
    if user_form.id.data:
        user = User.query.filter_by(id=user_form.id.data).first()

        if request.method == "POST" and user_form.submit():
            user.fname = user_form.fname.data
            user.lname=user_form.lname.data
            user.password=user_form.password.data
            user.username=user_form.username.data
            user.term=user_form.term.data
  
            db.session.commit()
            flash("اطلاعات جدید ویرایش شد.")
            return redirect(url_for('main.users'))
    
        return render_template('list_users.html', form=user_form, title=title, users=users)


@main.route('/users/delete/<int:id>',  methods=["GET"])
@login_required
def delete_user(id):
    title = "حذف کاربر"
    if type(id) == int:
        user = User.query.filter_by(id=id).first()
        if user.username == "admin":
            flash("کاربر مدیر را نمیتوانید حذف کنید.")
            return redirect(url_for('main.users'))
        else:
            db.session.delete(user)
            db.session.commit()
            flash("کاربر حذف شد.")
            return redirect(url_for('main.users'))


